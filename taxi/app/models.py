from django.db import models

# Create your models here.

# --- examples ---


# class IntegerBox(models.Model):
#     integer = models.IntegerField()

#     def integer_value(self):
#         return self.integer


# --- project ---


class Client(models.Model):
    first_name = models.CharField(max_length=400)
    last_name = models.CharField(max_length=400)
    email = models.EmailField(max_length=254)
    associated_card_number = models.CharField(max_length=16, null=True)
    # debit or credit card
    associated_card_type = models.CharField(max_length=400, null=True)
    score = models.FloatField()

    def __str__(self) -> str:
        full_name = self.first_name + " " + self.last_name
        return full_name


class Driver(models.Model):
    first_name = models.CharField(max_length=400)
    last_name = models.CharField(max_length=400)
    email = models.EmailField(max_length=254)
    rut = models.IntegerField()
    bank = models.CharField(max_length=400)
    bank_account_number = models.CharField(max_length=16)
    car_plate = models.CharField(max_length=6)
    car_brand = models.CharField(max_length=400)
    car_model = models.CharField(max_length=400)
    car_color = models.CharField(max_length=400)
    score = models.FloatField()

    def __str__(self) -> str:
        full_name = self.first_name + " " + self.last_name
        return full_name


class Trip(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE,null=True)
    # cost of entire trip in chilean pesos
    cost = models.IntegerField()
    created_at = models.DateTimeField()
    # payment options
    payment = models.CharField(max_length=400)
    # trip completion state
    completion_state = models.CharField(max_length=400)
    score = models.FloatField(null=True)

    def __str__(self) -> str:
        tr = f"Id: {self.pk}, Created at: {self.created_at}, Client: {self.client}, Driver: {self.driver}"
        return tr


class Route(models.Model):
    lat_coord = models.FloatField()
    lon_coord = models.FloatField()
    timestamp = models.DateTimeField()
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE)

    class Meta:
        ordering = ["timestamp"]

    def __str__(self) -> str:
        route_resume = f"Trip Id: {self.trip_id}, Timestamp: {self.timestamp}"
        return route_resume


# Background data of drivers that apply to taxi company.
class BackgroundCheck(models.Model):
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    age = models.IntegerField()
    criminal_record = models.FileField(upload_to="criminal_record/")
    profile_headshot = models.FileField(upload_to="profile_headshot/")
    id_photo = models.FileField(upload_to="id_photos/")
    """ status of background check procedure:
        processing, accepted, rejected"""
    status_of_procedure = models.CharField(max_length=400)

    def __str__(self) -> str:
        return f"Driver Id: {self.driver_id}"


# Payments of trips taken by clients
class ClientPayment(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE)
    amount = models.IntegerField(null=True)
    payment_method = models.CharField(max_length=400, null=True)
    """ status of transaction:
        pending, accepted, declined"""
    status = models.CharField(max_length=400)

    def __str__(self) -> str:
        payment_resume = f"Client Id: {self.client_id}, Trip Id: {self.trip_id}"
        return payment_resume


# Payments of trips done by drivers
class DriverPayment(models.Model):
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE)
    amount = models.IntegerField(null=True)
    bank = models.CharField(max_length=400, null=True)
    bank_account_number = models.CharField(max_length=16, null=True)
    """ status of transaction:
        pending, accepted, declined"""
    status = models.CharField(max_length=400)

    def __str__(self) -> str:
        payment_resume = f"Driver Id: {self.driver_id}, Trip Id: {self.trip_id}"
        return payment_resume


class RideChat(models.Model):
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE)
    chat = models.JSONField()
